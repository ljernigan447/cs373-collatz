#!/usr/bin/env python3

# ----------
# Collatz.py
# ----------

# pylint: disable = invalid-name
# pylint: disable = missing-docstring

# -------
# imports
# -------

from typing import Dict, IO, Tuple

cache: Dict[int, int] = {}

# ------------
# collatz_read
# ------------


def collatz_read(s: str) -> Tuple[int, int]:
    """
    read two ints
    s a string
    return a tuple of two ints
    """
    a = s.split()
    return int(a[0]), int(a[1])


# ------------
# collatz_eval
# ------------


def collatz_eval(t: Tuple[int, int]) -> Tuple[int, int, int]:
    """
    t a tuple of two ints
    return a tuple of three ints
    """
    i, j = t
    assert i > 0 and j > 0
    lower, upper = t
    # Switch so that lower is less than upper
    if lower > upper:
        upper, lower = t
    # Optimization so we only have to check from the middle to the upper bound
    if lower < upper >> 2:
        lower = upper >> 2
    mx: int = 0
    upper += 1
    for x in range(lower, upper):
        if x in cache:
            # Check if value is in cache
            val: int = cache[x]
        else:
            # Find the cycle length
            val = collatz_find_cycles(x)
            cache[x] = val
        if val > mx:
            mx = val
    assert mx > 0 and isinstance(i, int) and isinstance(j, int) and isinstance(mx, int)
    return i, j, mx


# ------------
# collatz_find_cycles
# ------------


def collatz_find_cycles(n: int) -> int:
    """
    n an int that you want to find the cycle length of
    return an int that is the cycle length
    """
    assert n > 0
    count = 1
    while n > 1:
        if n in cache:
            # Try to grab from cache
            count = count + cache[n] - 1
            n = 1
        else:
            if n & 1:
                # Odd case, optimized even case
                n = n + (n >> 1) + 1
                count += 2
            else:
                # Even case
                n = n >> 1
                count += 1
    return count


# -------------
# collatz_print
# -------------


def collatz_print(sout: IO[str], t: Tuple[int, int, int]) -> None:
    """
    print three ints
    sout a writer
    t a tuple of three ints
    """
    i, j, v = t
    sout.write(str(i) + " " + str(j) + " " + str(v) + "\n")


# -------------
# collatz_solve
# -------------


def collatz_solve(sin: IO[str], sout: IO[str]) -> None:
    """
    sin  a reader
    sout a writer
    """
    for s in sin:
        for n in range(2, 100):
            cache[n] = collatz_find_cycles(n)
        collatz_print(sout, collatz_eval(collatz_read(s)))
