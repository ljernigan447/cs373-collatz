#!/usr/bin/env python3

from random import randint


def main():
    for x in range(0, 250):
        rand_A: int = randint(0, 1000000)
        rand_B: int = randint(0, 1000000)
        while rand_B < rand_A:
            rand_B = randint(0, 1000000)

        print(f"{rand_A} {rand_B}")


if __name__ == "__main__":
    main()
