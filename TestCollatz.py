#!/usr/bin/env python3

# --------------
# TestCollatz.py
# --------------

# pylint: disable = invalid-name
# pylint: disable = missing-docstring

# -------
# imports
# -------
# Comment  something else
from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz(TestCase):
    # ----
    # read
    # ----

    def test_read(self):
        self.assertEqual(collatz_read("1 10\n"), (1, 10))

    # ----
    # eval
    # ----

    def test_eval_1(self):
        self.assertEqual(collatz_eval((1, 10)), (1, 10, 20))

    def test_eval_2(self):
        self.assertEqual(collatz_eval((100, 200)), (100, 200, 125))

    def test_eval_3(self):
        self.assertEqual(collatz_eval((201, 210)), (201, 210, 89))

    def test_eval_4(self):
        self.assertEqual(collatz_eval((900, 1000)), (900, 1000, 174))

    # --------
    # My tests
    # --------

    def test_my_eval_01(self):
        self.assertEqual(collatz_eval((1, 2)), (1, 2, 2))

    def test_my_eval_02(self):
        self.assertEqual(collatz_eval((1, 1)), (1, 1, 1))

    def test_my_eval_03(self):
        self.assertEqual(collatz_eval((999999, 999999)), (999999, 999999, 259))

    def test_my_eval_04(self):
        self.assertEqual(collatz_eval((500, 5000)), (500, 5000, 238))

    def test_my_eval_05(self):
        self.assertEqual(collatz_eval((10, 1)), (10, 1, 20))

    def test_my_eval_06(self):
        self.assertEqual(collatz_eval((1, 999999)), (1, 999999, 525))

    def test_my_eval_07(self):
        with self.assertRaises(AssertionError):
            self.assertEquals(collatz_eval((0, 0)), (0, 0, 1))

    def test_my_eval_08(self):
        self.assertEqual(collatz_eval((999863, 999999)), (999863, 999999, 259))

    def test_my_eval_09(self):
        with self.assertRaises(AssertionError):
            self.assertEqual(collatz_eval((-1, -10)), (-1, -10, 20))

    def test_my_eval_10(self):
        self.assertNotEqual(collatz_eval((1, 10)), (1, 10, 120))

    # -----
    # print
    # -----

    def test_print(self):
        sout = StringIO()
        collatz_print(sout, (1, 10, 20))
        self.assertEqual(sout.getvalue(), "1 10 20\n")

    # -----
    # solve
    # -----

    def test_solve(self):
        sin = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        sout = StringIO()
        collatz_solve(sin, sout)
        self.assertEqual(
            sout.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n"
        )


# ----
# main
# ----

if __name__ == "__main__":  # pragma: no cover
    main()
